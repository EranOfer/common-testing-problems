# Common testing problems

Exercises to increase awareness of common testing problems

# What is the project about
This project gives a set of real-life examples of tests that not validate the contract properly.

# What is it for me 
As you do the various exercises you will gain an understanding of good and bad test patterns. 


Requirement - Set of features and beavers that the software should include.  
Contract - a written or spoken agreement, this set of explanations that when they changed can lead to bug in the consumer side.  
Implementation - a way to express the requirement and the beavers.

Note: The Implementation and requirement tend to change, but the inner contract between system and class when abstract correctly tend to stay the same.    

# Let define test objective in this exercise

* Brake when the contract changed.
* Test the beavers and not the implementation.
* Clear meaning
...

