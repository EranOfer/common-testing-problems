package SimpleService

import "gitlab.com/common-testing-problems/SimpleService/thirdPartyService"

type UserService struct {
	externalDependency thirdPartyService.ExternalDependencyService
}

func CreateService(externalDependency thirdPartyService.ExternalDependencyService) UserService {
	return UserService{externalDependency: externalDependency}
}

func (s UserService) CreateUser(student User) error {
	var newStudent = thirdPartyService.User{Id: student.Id, Phone: student.Phone, LastName: student.Phone}
	err := s.externalDependency.CreateUser(newStudent)
	return err

}

func (s *UserService) GetUser(id string) (*User, error) {
	result, err := s.externalDependency.GetUser(id)
	if err != nil {
		return nil, err
	}
	return &User{Id: result.Id, LastName: result.LastName, Phone: result.Phone}, nil
}

type User struct {
	Id       string
	Name     string
	LastName string
	Phone    string
}
