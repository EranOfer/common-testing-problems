package thirdPartyService

type User struct {
	Id       string
	Name     string
	LastName string
	Phone    string
}

type ExternalDependencyService interface {
	GetUser(userId string) (*User, error)
	CreateUser(student User) error
}

type ExternalDependencyServiceError string

func (externalDependencyServiceError ExternalDependencyServiceError) Error() string {
	return string(externalDependencyServiceError)
}

const ErrorUserNotExists ExternalDependencyServiceError = "User not exist"
