package TestUtils

import (
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/common-testing-problems/SimpleService"
	"gitlab.com/common-testing-problems/SimpleService/thirdPartyService"
	"testing"
)

func GetNewStudent() SimpleService.User {

	return SimpleService.User{Id: uuid.NewV4().String(), Name: "jon", LastName: "do", Phone: "050404040404"}
}

func CreateUser(t *testing.T, service SimpleService.UserService) SimpleService.User {
	var user = GetNewStudent()
	err := service.CreateUser(user)
	require.NoError(t, err)
	return user
}

func CreateFakeUserService() SimpleService.UserService {
	fakeDependency := fakeDataBase{memoryStorage: make(map[string]thirdPartyService.User)}
	return SimpleService.CreateService(fakeDependency)
}

type fakeDataBase struct {
	memoryStorage map[string]thirdPartyService.User
}

func (fakeDataBase fakeDataBase) GetUser(userId string) (*thirdPartyService.User, error) {
	user, exists := fakeDataBase.memoryStorage[userId]
	if exists == false {
		return nil, thirdPartyService.ErrorUserNotExists
	}

	return &user, nil
}
func (fakeDataBase fakeDataBase) CreateUser(student thirdPartyService.User) error {
	fakeDataBase.memoryStorage[student.Id] = student
	return nil
}
