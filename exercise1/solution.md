# Solution

The problem is the test not validate the contract currently.
There are multiple bugs in the mapping between the UserService and ExternalDependencyService.

I encounter this kind of problem in real life when the test not validate the contract as expected.

```go
func Test_Get_Existing_User_Should_Return_User(t *testing.T) {
	var service = CreateFakeUserService()
	// Given
	expectedUser := CreateUser(t, service)
	// When
	actual, err := service.GetUser(expectedUser.Id)
	// Then
	require.NoError(t, err)
	require.EqualValues(t, expectedUser, actual)
}
```
