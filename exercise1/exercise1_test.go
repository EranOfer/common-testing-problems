package exercise1

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/common-testing-problems/TestUtils"
	"testing"
)

/*
   This test is not validate the contract.
   You should fix the test according to instructions that found in this directory.
*/

func Test_Get_Existing_User_Should_Return_User(t *testing.T) {
	var service = TestUtils.CreateFakeUserService()
	// Given
	user := TestUtils.CreateUser(t, service)
	// When
	_, err := service.GetUser(user.Id)
	// Then
	require.NoError(t, err)
}
