# Exercise 1 Instructions


There is a bug in our service implantation, but this test is still passing.  

1. Refactor the "Then" section in exercise1_test.go when your code is correct the test should fail.
2. Fix the service making the test pass.
3. Copy the test from the solution to exercise1_test.go make sure it pass.

