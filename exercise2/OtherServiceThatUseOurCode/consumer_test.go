package exercise2

import (
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/common-testing-problems/TestUtils"
	"testing"
)

// Track bad login consumer test out of you control
func Test_WhenNotValidID_ShouldSendMailToAdministrator(t *testing.T) {
	var service = TestUtils.CreateFakeUserService()
	mock := &MockMailDependency{}

	// Given
	notExistsUser := uuid.NewV4().String()
	ip := "1404.4.4"
	mock.On("SendMail", notExistsUser, ip).Return(nil)
	// When
	_, err := service.GetUser(notExistsUser)
	UserServiceErrorHandling(err, notExistsUser, ip, mock)
	// Then
	require.Equal(t, 1, len(mock.ExpectedCalls))
}
