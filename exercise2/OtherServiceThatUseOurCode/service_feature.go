package exercise2

// this code is one on the inner requirement of service the consume your code
func UserServiceErrorHandling(error error, userID string, ip string, mailDependency MailDependency) {

	if error.Error() == "User not exist" {
		mailDependency.SendMail(userID, ip)
	}
}
