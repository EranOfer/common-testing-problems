package exercise2

// This code is exemple of a code you can not control that consume you code
//go:generate mockery -name MailDependency -inpkg -case=underscore -output MockMailDependency

type MailDependency interface {
	SendMail(userID string, ip string) error
}
