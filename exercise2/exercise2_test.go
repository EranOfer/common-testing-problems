package exercise2

import (
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/common-testing-problems/TestUtils"
	"testing"
)

/*
   This test is not validate the contract.
   You should fix the test according to instructions that found in this directory.
*/
func Test_GetStudent_WhenNotValidID_Should_Fail(t *testing.T) {
	var service = TestUtils.CreateFakeUserService()

	// Given
	notExistsUser := uuid.NewV4().String()

	// When
	_, err := service.GetUser(notExistsUser)

	// Then
	require.Error(t, err)
}
