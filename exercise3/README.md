# Exercise 3 Instructions

When running tests without the required credential of few tests, I notice the all of them start to failed.
In my case it happened due to the use of testify.assert that continue to run the test even without the required permission.

Casing log.fatal that trigger os.exits and terminate all the tests. 

I refactor all my test to use testify.require instead it make my test more stable.

require package is very similar to assert, except for it terminates test if assertion is not successful. 

refactor the test to use testify.require and all the test should run and only one fail!!

